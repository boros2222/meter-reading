package com.example.meterreading.domain;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "addresses")
public class Address {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column
    private Integer zipCode;
    @Column
    private String city;
    @Column
    private String street;
    @Column
    private String house;

    @OneToOne(mappedBy = "address")
    private Client client;

    public Address() {
    }

    public Address(Integer zipCode, String city, String street, String house) {
        this.zipCode = zipCode;
        this.city = city;
        this.street = street;
        this.house = house;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getZipCode() {
        return zipCode;
    }

    public void setZipCode(Integer zipCode) {
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        Address address = (Address) o;
        return id.equals(address.id) &&
                zipCode.equals(address.zipCode) &&
                city.equals(address.city) &&
                street.equals(address.street) &&
                house.equals(address.house) &&
                client.equals(address.client);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, zipCode, city, street, house, client);
    }
}
