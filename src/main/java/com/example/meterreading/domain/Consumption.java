package com.example.meterreading.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;

@Entity
@Table(name = "consumptions")
public class Consumption {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column
    private LocalDate date;
    @Column
    private Month month;
    @Column
    private Integer value;

    @JsonBackReference
    @ManyToOne
    private Meter meter;

    public Consumption() {
    }

    public Consumption(YearMonth yearMonth, Integer value, Meter meter) {
        this.date = yearMonth.atDay(1);
        this.month = yearMonth.getMonth();
        this.value = value;
        this.meter = meter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Month getMonth() {
        return month;
    }

    public void setMonth(Month month) {
        this.month = month;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Meter getMeter() {
        return meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }
}