package com.example.meterreading.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "meters")
public class Meter {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "meter")
    private List<Consumption> consumptions;

    @OneToOne(mappedBy = "meter")
    private Client client;

    public Meter() {
    }

    public Meter(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Consumption> getConsumptions() {
        return consumptions;
    }

    public void setConsumptions(List<Consumption> consumptions) {
        this.consumptions = consumptions;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }
}
