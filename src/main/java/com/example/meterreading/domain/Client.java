package com.example.meterreading.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(
        name = "clients",
        uniqueConstraints=@UniqueConstraint(columnNames={"address_id", "meter_id"})
)
public class Client {

    @GeneratedValue(strategy = GenerationType.AUTO)
    @Id
    private Long id;

    @Column
    private String firstName;
    @Column
    private String lastName;

    @JsonBackReference
    @OneToOne
    @JoinColumn(name = "address_id", referencedColumnName = "id")
    private Address address;

    @JsonBackReference
    @OneToOne
    @JoinColumn(name = "meter_id", referencedColumnName = "id")
    private Meter meter;

    public Client() {
    }

    public Client(String firstName, String lastName, Address address, Meter meter) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.meter = meter;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Meter getMeter() {
        return meter;
    }

    public void setMeter(Meter meter) {
        this.meter = meter;
    }
}
