package com.example.meterreading.repository;

import com.example.meterreading.domain.Meter;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MeterRepository extends CrudRepository<Meter, Long> {

    List<Meter> findAll();

    @Query(value = "SELECT * FROM meters WHERE id = :id", nativeQuery = true)
    Optional<Meter> findById(@Param("id") Long id);
}
