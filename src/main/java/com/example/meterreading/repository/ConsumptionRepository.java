package com.example.meterreading.repository;

import com.example.meterreading.domain.Consumption;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface ConsumptionRepository extends CrudRepository<Consumption, Long> {

    List<Consumption> findAll();

    @Query(value = "SELECT * FROM consumptions WHERE meter_id = :id AND date >= :start AND date <= :end", nativeQuery = true)
    List<Consumption> findAllByMeterIdByDateBetween(@Param("id") Long id,
                                                    @Param("start") LocalDate start,
                                                    @Param("end") LocalDate end);
}
