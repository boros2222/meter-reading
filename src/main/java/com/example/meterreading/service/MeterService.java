package com.example.meterreading.service;

import com.example.meterreading.domain.Address;
import com.example.meterreading.domain.Client;
import com.example.meterreading.domain.Consumption;
import com.example.meterreading.domain.Meter;
import com.example.meterreading.repository.AddressRepository;
import com.example.meterreading.repository.ClientRepository;
import com.example.meterreading.repository.ConsumptionRepository;
import com.example.meterreading.repository.MeterRepository;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.persistence.EntityExistsException;
import java.time.LocalDate;
import java.time.YearMonth;
import java.util.List;
import java.util.NoSuchElementException;

@Service
public class MeterService {

    private AddressRepository addressRepository;
    private ClientRepository clientRepository;
    private ConsumptionRepository consumptionRepository;
    private MeterRepository meterRepository;

    /** Adding some initial data to database through Java */
    @PostConstruct
    public void init() {
        Address address = new Address(4000,"Debrecen","Raktár utca","8.");
        addressRepository.save(address);

        Meter meter = new Meter("ElectroMeter 2000");
        meterRepository.save(meter);

        Consumption c1 = new Consumption(YearMonth.of(2019,4),19, meter);
        consumptionRepository.save(c1);
        Consumption c2 = new Consumption(YearMonth.of(2019, 5),45, meter);
        consumptionRepository.save(c2);
        Consumption c3 = new Consumption(YearMonth.of(2017, 8),67, meter);
        consumptionRepository.save(c3);

        Client client = new Client("Elek","Teszt", address, meter);
        clientRepository.save(client);

        System.out.println("Initial data loaded to database!");
    }

    /**
     * @return all meters and their consumptions
     */
    public List<Meter> getConsumptions() {
        return meterRepository.findAll();
    }

    public Meter getConsumptions(Long meterId, Integer year) {
        return getDataByMeterByYear(meterId, year);
    }

    public Meter getConsumptions(Long meterId, Integer year, Integer month) {
        return getDataByMeterByYearByMonth(meterId, year, month);
    }

    public String getTotalConsumption(Long meterId, Integer year) {
        Meter meter = getDataByMeterByYear(meterId, year);
        Integer total = meter.getConsumptions().stream().mapToInt(Consumption::getValue).sum();

        JSONObject json = new JSONObject();
        json.put("meter_id", meterId);
        json.put("year", year);
        json.put("total_consumption", total);

        return json.toString();
    }

    public void addConsumption(Long meterId, Integer year, Integer month, Integer value) throws EntityExistsException {
        Meter meter = getDataByMeterByYearByMonth(meterId, year, month);

        if(meter.getConsumptions().size() == 0) {
            Consumption consumption = new Consumption(YearMonth.of(year, month), value, meter);
            consumptionRepository.save(consumption);
        }
        else {
            throw new EntityExistsException("This month of the year already has a consumption for this meter.");
        }
    }

    /**
     * @return meter by id and its consumptions by year
     */
    public Meter getDataByMeterByYear(Long meterId, Integer year) {
        LocalDate startDate = LocalDate.of(year, 1, 1);
        LocalDate endDate = LocalDate.of(year, 12, 31);

        return getDataByMeterByDateBetween(meterId, startDate, endDate);
    }

    /**
     * @return meter by id and its consumption by year by month
     */
    public Meter getDataByMeterByYearByMonth(Long meterId, Integer year, Integer month) {
        LocalDate startDate = LocalDate.of(year, month, 1);
        LocalDate endDate = startDate.withDayOfMonth(startDate.getMonth().length(startDate.isLeapYear()));

        return getDataByMeterByDateBetween(meterId, startDate, endDate);
    }

    /**
     * @return meter by id and its consumptions between given dates
     */
    public Meter getDataByMeterByDateBetween(Long meterId, LocalDate start, LocalDate end) throws NoSuchElementException {
        Meter meter = meterRepository.findById(meterId).orElse(null);

        if(meter == null)
            throw new NoSuchElementException("Meter with this id is not found.");

        List<Consumption> consumptions = consumptionRepository.findAllByMeterIdByDateBetween(meterId, start, end);
        meter.setConsumptions(consumptions);

        return meter;
    }

    @Autowired
    public void setAddressRepository(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Autowired
    public void setClientRepository(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    @Autowired
    public void setConsumptionRepository(ConsumptionRepository consumptionRepository) {
        this.consumptionRepository = consumptionRepository;
    }

    @Autowired
    public void setMeterRepository(MeterRepository meterRepository) {
        this.meterRepository = meterRepository;
    }
}
