package com.example.meterreading.controller;

import com.example.meterreading.domain.Meter;
import com.example.meterreading.service.MeterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ApiController {

    private MeterService meterService;

    @GetMapping("/consumptions")
    public List<Meter> getAllConsumptions() {

        return meterService.getConsumptions();
    }

    @GetMapping("/consumptions/{meter-id}/{year}")
    public Meter getConsumptionsByYear(@PathVariable(value = "meter-id") Long meterId,
                                       @PathVariable(value = "year") Integer year) {

        return meterService.getConsumptions(meterId, year);
    }

    @GetMapping("/consumptions/{meter-id}/{year}/{month}")
    public Meter getConsumptionsByYearByMonth(@PathVariable(value = "meter-id") Long meterId,
                                              @PathVariable(value = "year") Integer year,
                                              @PathVariable(value = "month") Integer month) {

        return meterService.getConsumptions(meterId, year, month);
    }

    @GetMapping("/consumptions/total/{meter-id}/{year}")
    public String getTotalConsumptionByYear(@PathVariable(value = "meter-id") Long meterId,
                                            @PathVariable(value = "year") Integer year) {

        return meterService.getTotalConsumption(meterId, year);
    }

    @PostMapping("/consumptions")
    public void addConsumptionToSpecificMeter(@RequestParam(value = "meter-id") Long meterId,
                                              @RequestParam(value = "year") Integer year,
                                              @RequestParam(value = "month") Integer month,
                                              @RequestParam(value = "value") Integer value) {

        meterService.addConsumption(meterId, year, month, value);
    }

    @Autowired
    public void setMeterService(MeterService meterService) {
        this.meterService = meterService;
    }
}
