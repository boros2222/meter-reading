package com.example.meterreading.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
public class ErrController implements ErrorController {

    private static final String ERR_PATH = "/error";
    private ErrorAttributes errorAttributes;

    @GetMapping(ERR_PATH)
    public Object error(HttpServletRequest request) {
        ServletWebRequest sWR = new ServletWebRequest(request);
        Map<String, Object> error = this.errorAttributes.getErrorAttributes(sWR,true);

        return error.get("message");
    }

    @Override
    public String getErrorPath() {
        return ERR_PATH;
    }

    @Autowired
    public void setErrorAttributes(ErrorAttributes errorAttributes) {
        this.errorAttributes = errorAttributes;
    }
}
