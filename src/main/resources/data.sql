
-- Adding some initial data

INSERT INTO addresses (id, zip_code, city, street, house)
VALUES (5, 4400, 'Nyíregyháza', 'Szarvas utca', '2.');

INSERT INTO meters (id, name)
VALUES (99, 'Super Meter 3.0');

INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (10, to_date('2016-01-01','YYYY-MM-DD'), 0, 49, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (11, to_date('2016-02-01','YYYY-MM-DD'), 1, 34, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (12, to_date('2016-03-01','YYYY-MM-DD'), 2, 72, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (13, to_date('2016-04-01','YYYY-MM-DD'), 3, 87, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (14, to_date('2016-05-01','YYYY-MM-DD'), 4, 58, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (15, to_date('2016-06-01','YYYY-MM-DD'), 5, 68, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (16, to_date('2016-07-01','YYYY-MM-DD'), 6, 22, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (17, to_date('2016-08-01','YYYY-MM-DD'), 7, 16, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (18, to_date('2016-09-01','YYYY-MM-DD'), 8, 43, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (19, to_date('2016-10-01','YYYY-MM-DD'), 9, 108, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (20, to_date('2016-11-01','YYYY-MM-DD'), 10, 72, 99);
INSERT INTO consumptions (id, date, month, value, meter_id)
VALUES (21, to_date('2016-12-01','YYYY-MM-DD'), 11, 51, 99);

INSERT INTO clients (id, first_name, last_name, address_id, meter_id)
VALUES (8, 'Péter', 'Pék', 5, 99);