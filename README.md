**Spring Boot Application for processing metering data**  
This application is created with Java version 11 and using H2 In-Memory Database for storing data.
---

## Run application

1. Clone project
2. Build application: **`mvn package`**
3. Run application: **`java -jar target/<application>.jar`**

---

## Usage

The application runs on port 4000 by default.

**Routes**  
- GET Routes:  
**`http://localhost:4000/consumptions`** - Lists all the meters and their consumptions from database.  
**`http://localhost:4000/consumptions/<meter-id>/<year>`** - Lists consumption per month to given meter and year.  
**`http://localhost:4000/consumptions/<meter-id>/<year>/<month-in-number>`** - Lists consumption to given meter and month of the year.  
**`http://localhost:4000/consumptions/total/<meter-id>/<year>`** - Returns total consumption to given meter and year  
- POST Routes:  
**`http://localhost:4000/consumptions`** - Adds consumption to database for specific meter, year and month. Field names need to be provided: **`meter-id`**, **`year`**, **`month`**, **`value`**.  
